/*
   +----------------------------------------------------------------------+
   | Copyright (c) The PHP Group                                          |
   +----------------------------------------------------------------------+
   | This source file is subject to version 3.01 of the PHP license,      |
   | that is bundled with this package in the file LICENSE, and is        |
   | available through the world-wide-web at the following url:           |
   | http://www.php.net/license/3_01.txt                                  |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
   | Authors: Gina Peter Banyard <girgias@php.net>                        |
   +----------------------------------------------------------------------+
*/
/* csv extension for PHP */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "php.h"
#include "ext/standard/info.h"
#include "ext/standard/php_string.h" /* For php_str_to_str() */
#include "php_csv.h"
#include "csv_arginfo.h"

#include <stdbool.h>
#include "zend_smart_str.h"
#include "Zend/zend_interfaces.h"

/* Compatibility */
#if PHP_VERSION_ID < 80400
static void zend_argument_must_not_be_empty_error(uint32_t arg_num)
{
	zend_argument_value_error(arg_num, "must not be empty");
}
#endif

PHP_MINFO_FUNCTION(csv)
{
	php_info_print_table_start();
		php_info_print_table_header(2, "CSV support", "enabled");
		php_info_print_table_header(2, "Version", PHP_CSV_VERSION);
		php_info_print_table_header(2, "Author", "Gina Peter Banyard");
		php_info_print_table_header(2, "Bug reports", PHP_CSV_BUGREPORT);
		php_info_print_table_header(2, "Sponsor me", "https://github.com/sponsors/Girgias");
	php_info_print_table_end();
}

#define EOL_SEQUENCE_AND_DELIMITER_AND_ENCLOSURE_CHECKS(delimiter_arg_num, enclosure_arg_num, eol_arg_num) {	\
	if (eol_sequence) {																							\
		/* Make sure that there is at least one character in string */											\
		if (UNEXPECTED(ZSTR_LEN(eol_sequence) == 0)) {															\
			zend_argument_must_not_be_empty_error(eol_arg_num);													\
			RETURN_THROWS();																					\
		}																										\
	} else {																									\
		eol_sequence = zend_string_init(ZEND_STRL("\r\n"), 0);													\
	}																											\
																												\
	if (delimiter) {																							\
		/* Make sure that there is at least one character in string */											\
		if (UNEXPECTED(ZSTR_LEN(delimiter) == 0)) {																\
			zend_argument_must_not_be_empty_error(delimiter_arg_num);											\
			zend_string_release(eol_sequence);																	\
			RETURN_THROWS();																					\
		}																										\
		if (UNEXPECTED(zend_string_equals(delimiter, eol_sequence))) {											\
			zend_argument_value_error(																			\
				eol_arg_num,																					\
				"must not be identical to argument #%"PRIu32" ($delimiter)",									\
				delimiter_arg_num);																				\
			zend_string_release(eol_sequence);																	\
			RETURN_THROWS();																					\
		}																										\
	} else {																									\
		delimiter = ZSTR_CHAR(',');																				\
	}																											\
																												\
	if (enclosure) {																							\
		if (UNEXPECTED(ZSTR_LEN(enclosure) == 0)) {																\
			zend_argument_must_not_be_empty_error(enclosure_arg_num);											\
			zend_string_release(delimiter);																		\
			zend_string_release(eol_sequence);																	\
			RETURN_THROWS();																					\
		}																										\
		if (UNEXPECTED(zend_string_equals(enclosure, eol_sequence))) {											\
			zend_argument_value_error(																			\
				eol_arg_num,																					\
				"must not be identical to argument #%"PRIu32" ($enclosure)",									\
				enclosure_arg_num);																				\
			zend_string_release(eol_sequence);																	\
			zend_string_release(delimiter);																		\
			RETURN_THROWS();																					\
		}																										\
	} else {																									\
		enclosure = ZSTR_CHAR('"');																				\
	}																											\
																												\
	/* Ensure delimiter and enclosure are different */															\
	if (UNEXPECTED(zend_string_equals(delimiter, enclosure))) {													\
		zend_argument_value_error(																				\
			enclosure_arg_num,																					\
			"must not be identical to argument #%"PRIu32" ($delimiter)",										\
			delimiter_arg_num);																					\
		zend_string_release(eol_sequence);																		\
		zend_string_release(delimiter);																			\
		zend_string_release(enclosure);																			\
		RETURN_THROWS();																						\
	}																											\
}

/**
 * Follows RFC4180 https://tools.ietf.org/html/rfc4180
 * The terminology can be slightly confusing here as what PHP considers the 'enclosure' is what is used
 * to escape a field in the RFC.
 *
 * This only formats ONE row of a CSV file.
 *
 * Returns a NULL pointer on error
 */
static zend_string* hashtable_to_rfc4180_string(
	HashTable *fields,
	const zend_string *delimiter,
	const zend_string *enclosure,
	const zend_string *eol_sequence
) {
	uint32_t nb_fields;
	uint32_t fields_iterated = 0;
	zval *tmp_field_zval;
	smart_str row = {0};

	nb_fields = zend_hash_num_elements(fields);
	ZEND_HASH_FOREACH_VAL(fields, tmp_field_zval) {
		bool escape_field = false;
		bool enclosure_within_filed = false;
		zend_string *tmp_field_str;
		zend_string *field_str = zval_try_get_tmp_string(tmp_field_zval, &tmp_field_str);
		if (UNEXPECTED(field_str == NULL)) {
			smart_str_free(&row);
			return NULL;
		}

		/*
		 * A field must be escaped (enclosed) if it contains the delimiter OR a Carriage Return (\r)
		 * OR a Line Feed (\n) OR the field escape sequence (i.e. enclosure parameter) OR the custom EOL sequence
		 */
		if (
			memchr(ZSTR_VAL(field_str), '\n', ZSTR_LEN(field_str))
			|| memchr(ZSTR_VAL(field_str), '\r', ZSTR_LEN(field_str))
			|| php_memnstr(ZSTR_VAL(field_str), ZSTR_VAL(delimiter), ZSTR_LEN(delimiter),
				ZSTR_VAL(field_str) + ZSTR_LEN(field_str))
			|| php_memnstr(ZSTR_VAL(field_str), ZSTR_VAL(eol_sequence), ZSTR_LEN(eol_sequence),
				ZSTR_VAL(field_str) + ZSTR_LEN(field_str))
		) {
			escape_field = true;
		}
		/* If the field escape sequence (i.e. enclosure parameter) is within the field it needs to be
		 * duplicated within the field. */
		if (php_memnstr(ZSTR_VAL(field_str), ZSTR_VAL(enclosure), ZSTR_LEN(enclosure),
			ZSTR_VAL(field_str) + ZSTR_LEN(field_str))) {
			escape_field = true;
			enclosure_within_filed = true;
		}

		if (escape_field) {
			smart_str_append(&row, enclosure);

			if (enclosure_within_filed) {
				/* Create replace string (twice the field escape sequence) */
				smart_str escaped_enclosure = {0};
				smart_str_append(&escaped_enclosure, enclosure);
				smart_str_append(&escaped_enclosure, enclosure);
				smart_str_0(&escaped_enclosure);

				zend_string *replace = php_str_to_str(ZSTR_VAL(field_str), ZSTR_LEN(field_str), ZSTR_VAL(enclosure),
						ZSTR_LEN(enclosure), ZSTR_VAL(escaped_enclosure.s), ZSTR_LEN(escaped_enclosure.s));

				smart_str_append(&row, replace);

				smart_str_free(&escaped_enclosure);
				zend_string_release(replace);
			} else {
				smart_str_append(&row, field_str);
			}

			smart_str_append(&row, enclosure);
		} else {
			smart_str_append(&row, field_str);
		}

		/* Only add the delimiter in between fields on the same row. */
		if (++fields_iterated != nb_fields) {
			smart_str_append(&row, delimiter);
		}

		/* Clear temporary variable */
		zend_tmp_string_release(tmp_field_str);
	} ZEND_HASH_FOREACH_END();
	/* Add the EOL sequence to indicate the end of the row. */
	smart_str_append(&row, eol_sequence);

	smart_str_0(&row);
	return row.s;
}

static zend_object_iterator* csv_init_iterator_foreach(zval *iterator_zval, zval **first_iteration_value) {
	ZEND_ASSERT(Z_TYPE_P(iterator_zval) == IS_OBJECT);
	ZEND_ASSERT(first_iteration_value != NULL);

	zend_class_entry *ce = Z_OBJCE_P(iterator_zval);
	ZEND_ASSERT(ce->iterator_funcs_ptr != NULL);
	ZEND_ASSERT(ce->get_iterator != NULL);

	zend_object_iterator *it = ce->get_iterator(ce, iterator_zval, 0);
	if (UNEXPECTED(EG(exception))) {
		if (UNEXPECTED(it)) {
			zend_iterator_dtor(it);
		}
		return NULL;
	}
	ZEND_ASSERT(it != NULL);

	/* Rewind iterator */
	it->index = 0;
	if (it->funcs->rewind) {
		it->funcs->rewind(it);
		if (UNEXPECTED(EG(exception))) {
			zend_iterator_dtor(it);
			return NULL;
		}
		if (UNEXPECTED(EG(exception))) {
			zend_iterator_dtor(it);
			return NULL;
		}
	}

	if (it->funcs->valid(it) != SUCCESS || UNEXPECTED(EG(exception))) {
		zend_iterator_dtor(it);
		return NULL;
	}

	*first_iteration_value = it->funcs->get_current_data(it);
	if (*first_iteration_value == NULL || UNEXPECTED(EG(exception))) {
		zend_iterator_dtor(it);
		return NULL;
	}

	return it;
}

static zval* csv_advance_iterator_foreach(zend_object_iterator *it) {
	ZEND_ASSERT(it != NULL);

	/* Move iterator forward */
	it->index++;
	it->funcs->move_forward(it);
	if (UNEXPECTED(EG(exception))) {
		zend_iterator_dtor(it);
		return NULL;
	}

	if (it->funcs->valid(it) != SUCCESS || UNEXPECTED(EG(exception))) {
		zend_iterator_dtor(it);
		return NULL;
	}

	zval *next_value = it->funcs->get_current_data(it);
	if (next_value == NULL || UNEXPECTED(EG(exception))) {
		zend_iterator_dtor(it);
		return NULL;
	}

	return next_value;
}

#if PHP_VERSION_ID < 80200
#define CSV_ITERABLE_FOREACH_VAL(____iterable_zval, __value, __iterator_error_label) \
	do { \
		bool __is_array = true; \
		bool __is_foreach_loop_valid = true; \
		zval *__loop_value = NULL; \
		/* Needed for iterators */ \
		zend_object_iterator *__zend_iterator = NULL; \
		/* Copied from ZEND_HASH_FOREACH_VAL */ \
		const HashTable *__ht = NULL; \
		Bucket *_p = NULL; \
		const Bucket *_end = NULL; \
		if (Z_TYPE_P(____iterable_zval) == IS_ARRAY) { \
			__ht = Z_ARRVAL_P(____iterable_zval); \
			_p = __ht->arData; \
			_end = __ht->arData + __ht->nNumUsed; \
			__loop_value = &_p->val; \
		} else { \
			__is_array = false; \
			__zend_iterator = csv_init_iterator_foreach(____iterable_zval, &__loop_value); \
			if (UNEXPECTED(__zend_iterator == NULL)) { \
				goto __iterator_error_label; \
			} \
		} \
		while (__is_foreach_loop_valid) { \
			if (UNEXPECTED(Z_TYPE_P(__loop_value) == IS_UNDEF)) continue; \
			__value = __loop_value;

#define CSV_ITERABLE_FOREACH_END() \
			do { \
				if (__is_array) { \
					_p++; \
					__loop_value = &_p->val; \
					__is_foreach_loop_valid = _p != _end; \
				} else { \
					__loop_value = csv_advance_iterator_foreach(__zend_iterator); \
					if (__loop_value == NULL) { \
						__is_foreach_loop_valid = false; \
					} \
				} \
			} while (false); \
		} /* end while (__is_foreach_loop_valid) */ \
	} while (false)
#else
#define CSV_ITERABLE_FOREACH_VAL(____iterable_zval, __value, __iterator_error_label) \
	do { \
		bool __is_array = true; \
		bool __is_foreach_loop_valid = true; \
		zval *__loop_value = NULL; \
		/* Needed for iterators */ \
		zend_object_iterator *__zend_iterator = NULL; \
		/* Copied from _ZEND_HASH_FOREACH_VAL */ \
		const HashTable *__ht = NULL; \
		uint32_t _count = 0; \
		size_t _size = 0; \
		if (Z_TYPE_P(____iterable_zval) == IS_ARRAY) { \
			__ht = Z_ARRVAL_P(____iterable_zval); \
			_count = __ht->nNumUsed; \
			_size = ZEND_HASH_ELEMENT_SIZE(__ht); \
			__loop_value = __ht->arPacked; \
			__is_foreach_loop_valid = _count > 0; \
		} else { \
			__is_array = false; \
			__zend_iterator = csv_init_iterator_foreach(____iterable_zval, &__loop_value); \
			if (UNEXPECTED(__zend_iterator == NULL)) { \
				goto __iterator_error_label; \
			} \
		} \
		while (__is_foreach_loop_valid) { \
			if (UNEXPECTED(Z_TYPE_P(__loop_value) == IS_UNDEF)) continue; \
			__value = __loop_value;

#define CSV_ITERABLE_FOREACH_END() \
			do { \
				if (__is_array) { \
					_count--; \
					__loop_value = ZEND_HASH_NEXT_ELEMENT(__loop_value, _size); \
					__is_foreach_loop_valid = _count > 0; \
				} else { \
					__loop_value = csv_advance_iterator_foreach(__zend_iterator); \
					if (__loop_value == NULL) { \
						__is_foreach_loop_valid = false; \
					} \
				} \
			} while (false); \
		} /* end while (__is_foreach_loop_valid) */ \
	} while (false)
#endif

/* Returns a NULL pointer on error */
static HashTable* rfc4180_string_to_hashtable(
	const char **buffer,
	const char *end_buffer,
	const zend_string *delimiter,
	const zend_string *enclosure,
	const zend_string *eol_sequence
) {
	HashTable *return_value = zend_new_array(8);

	bool in_escaped_field = false;
	bool at_start_of_field = true;

	/* Dereference buffer */
	const char *row = *buffer;
	ZEND_ASSERT(row < end_buffer);

	smart_str field_value = {0};
	size_t field_value_length = 0;

	/* Main loop to "tokenize" the row */
	while (row < end_buffer) {
		/* Check for field escape sequence (i.e. enclosure) */
		if (php_memnstr(row, ZSTR_VAL(enclosure), ZSTR_LEN(enclosure), row + ZSTR_LEN(enclosure))) {
			row += ZSTR_LEN(enclosure);

			if (!in_escaped_field) {
				if (UNEXPECTED(!at_start_of_field)) {
					zend_value_error("Enclosure sequence is used in a non escaped field");
					smart_str_free(&field_value);
					zend_array_destroy(return_value);
					return NULL;
				}
				in_escaped_field = true;
				continue;
			}

			/* In an escaped field */
			if (php_memnstr(row, ZSTR_VAL(enclosure), ZSTR_LEN(enclosure), row + ZSTR_LEN(enclosure))) {
				in_escaped_field = true;
				goto consume;
			}

			in_escaped_field = false;
			continue;
		}

		/* Check for delimiter if not in an escaped field */
		if (
			!in_escaped_field
			&& php_memnstr(
				row,
				ZSTR_VAL(delimiter),
				ZSTR_LEN(delimiter),
				row + ZSTR_LEN(delimiter)
			)
		) {
			row += ZSTR_LEN(delimiter);

			/* Add nul terminating byte */
			smart_str_0(&field_value);
			zval tmp;
			ZVAL_STR_COPY(&tmp, field_value.s);
			zend_hash_next_index_insert(return_value, &tmp);

			smart_str_free(&field_value);

			field_value_length = 0;
			at_start_of_field = true;
			continue;
		}

		/* Check for End Of Line sequence when not in an escaped field */
		if (
			!in_escaped_field
			&& php_memnstr(
				row,
				ZSTR_VAL(eol_sequence),
				ZSTR_LEN(eol_sequence),
				row + ZSTR_LEN(eol_sequence)
			)
		) {
			row += ZSTR_LEN(eol_sequence);
			goto eol;
		}

		consume:
		at_start_of_field = false;
		smart_str_appendc(&field_value, row[0]);
		field_value_length++;
		row++;
	}

	eol:
	/* Add nul terminating byte */
	smart_str_0(&field_value);
	zval tmp;
	ZVAL_STR_COPY(&tmp, field_value.s);
	zend_hash_next_index_insert(return_value, &tmp);

	smart_str_free(&field_value);
	/* Update outer buffer position */
	*buffer = row;

	return return_value;
}

/* Returns a NULL pointer on error */
static HashTable* rfc4180_buffer_to_hashtable_collection(
	const zend_string *buffer,
	const zend_string *delimiter,
	const zend_string *enclosure,
	const zend_string *eol_sequence,
	bool is_lax
) {
	HashTable *return_value = zend_new_array(8);
	const char *start_position = ZSTR_VAL(buffer);
	const char *current_position = ZSTR_VAL(buffer);
	const char *end_buffer = ZSTR_VAL(buffer) + ZSTR_LEN(buffer);
	size_t length = ZSTR_LEN(buffer);
	size_t buffer_row_nb = 1;
	uint32_t nb_fields = 0;

	while (current_position - start_position < (ptrdiff_t) length) {
		uint32_t new_nb_fields = 0;
		HashTable *row = rfc4180_string_to_hashtable(&current_position, end_buffer, delimiter, enclosure, eol_sequence);

		/* Issue with parsing row */
		if (row == NULL) {
			zend_array_destroy(return_value);
			return NULL;
		}

		/* used to check if the number of fields is equal in each iteration */
		new_nb_fields = zend_hash_num_elements(row);
		/* buffer_row_nb == 1 means we are at the first iteration so don't check */
		if (new_nb_fields != nb_fields && buffer_row_nb != 1 && !is_lax) {
			zend_value_error("Buffer row %zu contains %"PRIu32" fields compared to %"PRIu32" fields on previous rows",
				buffer_row_nb, new_nb_fields, nb_fields);
			zend_array_destroy(row);
			zend_array_destroy(return_value);
			return NULL;
		}
		nb_fields = new_nb_fields;
		buffer_row_nb++;

		zval tmp;
		ZVAL_ARR(&tmp, row);
		zend_hash_next_index_insert(return_value, &tmp);
	}

	return return_value;
}

PHP_FUNCTION(Csv_array_to_row)
{
	zend_string *delimiter = NULL;
	zend_string *enclosure = NULL;
	zend_string *eol_sequence = NULL;
	HashTable *fields;

	if (zend_parse_parameters(ZEND_NUM_ARGS(), "h|SSS", &fields, &delimiter, &enclosure, &eol_sequence) == FAILURE) {
		RETURN_THROWS();
	}

	EOL_SEQUENCE_AND_DELIMITER_AND_ENCLOSURE_CHECKS(2, 3, 4);

	zend_string *result = hashtable_to_rfc4180_string(fields, delimiter, enclosure, eol_sequence);
	zend_string_release(eol_sequence);
	zend_string_release(delimiter);
	zend_string_release(enclosure);

	if (UNEXPECTED(result == NULL)) {
		RETURN_THROWS();
	}

	RETURN_STR(result);
}

PHP_FUNCTION(Csv_collection_to_buffer)
{
	zend_string *delimiter = NULL;
	zend_string *enclosure = NULL;
	zend_string *eol_sequence = NULL;
	zval *collection;

	ZEND_PARSE_PARAMETERS_START(1, 4)
		Z_PARAM_ITERABLE(collection)
		Z_PARAM_OPTIONAL
		Z_PARAM_STR(delimiter)
		Z_PARAM_STR(enclosure)
		Z_PARAM_STR(eol_sequence)
	ZEND_PARSE_PARAMETERS_END();

	EOL_SEQUENCE_AND_DELIMITER_AND_ENCLOSURE_CHECKS(2, 3, 4);

	zval *fields;
	size_t collection_index = 0;
	uint32_t nb_fields = 0;
	smart_str buffer = {0};
	bool has_errors = false;
	CSV_ITERABLE_FOREACH_VAL(collection, fields, end) {
		/* Check that fields is an array */
		if (UNEXPECTED(Z_TYPE_P(fields) != IS_ARRAY)) {
			zend_type_error("Element %zu of the collection must be an array", collection_index);
			has_errors = true;
			goto end;
		}

		/* used to check if the number of fields is equal in each iteration */
		uint32_t new_nb_fields = zend_hash_num_elements(Z_ARRVAL_P(fields));
		/* collection_index == 0 means we are at the first iteration so don't check */
		if (nb_fields != new_nb_fields && collection_index != 0) {
			zend_value_error("Element %zu of the collection contains %"PRIu32
			" fields compared to %"PRIu32" fields on previous rows",
				collection_index, new_nb_fields, nb_fields);
			has_errors = true;
			goto end;
		}
		nb_fields = new_nb_fields;
		collection_index++;

		zend_string *result = hashtable_to_rfc4180_string(Z_ARRVAL_P(fields), delimiter, enclosure, eol_sequence);
		if (UNEXPECTED(result == NULL)) {
			has_errors = true;
			goto end;
		}
		smart_str_append(&buffer, result);
		zend_string_release(result);
	} CSV_ITERABLE_FOREACH_END();

	end:
	/* Release strings */
	zend_string_release(eol_sequence);
	zend_string_release(delimiter);
	zend_string_release(enclosure);
	/* If an Error/Exception has been thrown */
	if (has_errors || UNEXPECTED(EG(exception))) {
		smart_str_free(&buffer);
		RETURN_THROWS();
	}

	smart_str_0(&buffer);
	RETURN_STR(buffer.s);
}

PHP_FUNCTION(Csv_row_to_array)
{
	zend_string *delimiter = NULL;
	zend_string *enclosure = NULL;
	zend_string *eol_sequence = NULL;
	zend_string *row;
	HashTable *fields = NULL;

	if (zend_parse_parameters(ZEND_NUM_ARGS(), "S|SSS", &row, &delimiter, &enclosure, &eol_sequence) == FAILURE) {
		RETURN_THROWS();
	}

	EOL_SEQUENCE_AND_DELIMITER_AND_ENCLOSURE_CHECKS(2, 3, 4);

	const char *row_c = ZSTR_VAL(row);
	const char *end_row = ZSTR_VAL(row) + ZSTR_LEN(row);
	fields = rfc4180_string_to_hashtable(&row_c, end_row, delimiter, enclosure, eol_sequence);
	zend_string_release(eol_sequence);
	zend_string_release(delimiter);
	zend_string_release(enclosure);

	if (UNEXPECTED(fields == NULL)) {
		RETURN_THROWS();
	}

	RETURN_ARR(fields);
}

static void buffer_to_collection_generic(INTERNAL_FUNCTION_PARAMETERS, bool is_lax)
{
	zend_string *delimiter = NULL;
	zend_string *enclosure = NULL;
	zend_string *eol_sequence = NULL;
	zend_string *buffer;

	if (zend_parse_parameters(ZEND_NUM_ARGS(), "S|SSS", &buffer, &delimiter, &enclosure, &eol_sequence) == FAILURE) {
		RETURN_THROWS();
	}

	EOL_SEQUENCE_AND_DELIMITER_AND_ENCLOSURE_CHECKS(2, 3, 4);

	HashTable *collection = rfc4180_buffer_to_hashtable_collection(buffer, delimiter, enclosure, eol_sequence, is_lax);

	/* Release strings */
	zend_string_release(eol_sequence);
	zend_string_release(delimiter);
	zend_string_release(enclosure);
	/* If an Error has been thrown */
	if (UNEXPECTED(collection == NULL)) {
		RETURN_THROWS();
	}
	RETURN_ARR(collection);
}

PHP_FUNCTION(Csv_buffer_to_collection)
{
	buffer_to_collection_generic(INTERNAL_FUNCTION_PARAM_PASSTHRU, false);
}
PHP_FUNCTION(Csv_buffer_to_collection_lax)
{
	buffer_to_collection_generic(INTERNAL_FUNCTION_PARAM_PASSTHRU, true);
}

static zend_class_entry *php_csv_lazy_collection_ce = NULL;
static zend_object_handlers php_csv_lazy_collection_object_handlers;

typedef struct php_csv_lazy_collection_object {
	zend_string *buffer;
	const char *buffer_current_position;
	zend_string *delimiter;
	zend_string *enclosure;
	zend_string *eol_sequence;
	/* Cannot use a HashTable as we need to be able to return a zval for current() */
	zval current_row;
	zend_object std;
} php_csv_lazy_collection_object;

static php_csv_lazy_collection_object *php_csv_lazy_collection_object_from(zend_object *object) {
	return (php_csv_lazy_collection_object *)(((char*) object) - XtOffsetOf(php_csv_lazy_collection_object, std));
}

static php_csv_lazy_collection_object *php_csv_lazy_collection_object_fetch(zval *obj) {
	return php_csv_lazy_collection_object_from(Z_OBJ_P(obj));
}

static zend_object *php_csv_lazy_collection_object_new(zend_class_entry *ce)
{
	php_csv_lazy_collection_object *lazy_collection = zend_object_alloc(sizeof(php_csv_lazy_collection_object), ce);
	zend_object_std_init(&lazy_collection->std, ce);
	object_properties_init(&lazy_collection->std, ce);
#if PHP_VERSION_ID < 80300
	// As default object handlers field only exists as of PHP 8.3, we need to set them during instantiation
	lazy_collection->std.handlers = &php_csv_lazy_collection_object_handlers;
#endif

	ZVAL_UNDEF(&lazy_collection->current_row);
	return &lazy_collection->std;
}

static void php_csv_lazy_collection_object_free(zend_object *std)
{
	php_csv_lazy_collection_object *lazy_collection = php_csv_lazy_collection_object_from(std);
	zval_ptr_dtor(&lazy_collection->current_row);

	/* PHP Will create an object even if php_csv_lazy_collection_get_constructor() throws,
	 * So we cannot assume the buffers and various CSV settings are set */
	if (EXPECTED(lazy_collection->buffer != NULL)) {
		zend_string_release(lazy_collection->buffer);
		lazy_collection->buffer = NULL;
	}

	if (EXPECTED(lazy_collection->delimiter != NULL)) {
		zend_string_release(lazy_collection->delimiter);
		lazy_collection->delimiter = NULL;
	}

	if (EXPECTED(lazy_collection->enclosure != NULL)) {
		zend_string_release(lazy_collection->enclosure);
		lazy_collection->enclosure = NULL;
	}

	if (EXPECTED(lazy_collection->eol_sequence != NULL)) {
		zend_string_release(lazy_collection->eol_sequence);
		lazy_collection->eol_sequence = NULL;
	}

	zend_object_std_dtor(&lazy_collection->std);
}

static zend_function *php_csv_lazy_collection_get_constructor(zend_object *obj) {
	zend_throw_error(NULL, "Cannot instantiate Csv\\LazyLaxCollection class");
	return NULL;
}

static const char* php_csv_lazy_collection_object_get_end_of_buffer(const php_csv_lazy_collection_object *lazy_collection) {
	return ZSTR_VAL(lazy_collection->buffer) + ZSTR_LEN(lazy_collection->buffer);
}

static bool php_csv_lazy_collection_is_buffer_at_eof(const php_csv_lazy_collection_object *lazy_collection) {
	return lazy_collection->buffer_current_position == php_csv_lazy_collection_object_get_end_of_buffer(lazy_collection);
}

/**
 * Csv\LazyLaxCollection internal iterator
 * Copied from zend_test/iterator.c
 */
typedef struct php_csv_lazy_collection_it {
	zend_object_iterator intern;
} php_csv_lazy_collection_it;

static php_csv_lazy_collection_it *php_csv_lazy_collection_it_fetch(zend_object_iterator *obj_iter) {
	return (php_csv_lazy_collection_it *)obj_iter;
}

static void php_csv_lazy_collection_it_dtor(zend_object_iterator *obj_iter) {
	php_csv_lazy_collection_it *iterator = php_csv_lazy_collection_it_fetch(obj_iter);
	zval_ptr_dtor(&iterator->intern.data);
}

static void php_csv_lazy_collection_it_next(zend_object_iterator *obj_iter) {
	php_csv_lazy_collection_it *iterator = php_csv_lazy_collection_it_fetch(obj_iter);
	php_csv_lazy_collection_object *lazy_collection = php_csv_lazy_collection_object_fetch(&iterator->intern.data);

	zval_ptr_dtor(&lazy_collection->current_row);
	/* Do not move past eof */
	if (php_csv_lazy_collection_is_buffer_at_eof(lazy_collection)) {
		ZVAL_UNDEF(&lazy_collection->current_row);
		return;
	}

	HashTable *row_ht = rfc4180_string_to_hashtable(
		&lazy_collection->buffer_current_position,
		php_csv_lazy_collection_object_get_end_of_buffer(lazy_collection),
		lazy_collection->delimiter,
		lazy_collection->enclosure,
		lazy_collection->eol_sequence
	);
	if (UNEXPECTED(row_ht == NULL)) {
		ZVAL_UNDEF(&lazy_collection->current_row);
		return;
	}

	ZVAL_ARR(&lazy_collection->current_row, row_ht);
}

static void php_csv_lazy_collection_it_rewind(zend_object_iterator *obj_iter) {
	php_csv_lazy_collection_it *iterator = php_csv_lazy_collection_it_fetch(obj_iter);
	php_csv_lazy_collection_object *lazy_collection = php_csv_lazy_collection_object_fetch(&iterator->intern.data);
	zval_ptr_dtor(&lazy_collection->current_row);
	lazy_collection->buffer_current_position = ZSTR_VAL(lazy_collection->buffer);

	/* Fetch first row as this is what is expected */
	php_csv_lazy_collection_it_next(obj_iter);
}

static zend_result php_csv_lazy_collection_it_valid(zend_object_iterator *obj_iter) {
	php_csv_lazy_collection_it *iterator = php_csv_lazy_collection_it_fetch(obj_iter);
	const php_csv_lazy_collection_object *lazy_collection = php_csv_lazy_collection_object_fetch(&iterator->intern.data);

	/* Upon reaching EOF the current row is freed and set to undef */
	return Z_ISUNDEF(lazy_collection->current_row) ? FAILURE : SUCCESS;
}

static zval *php_csv_lazy_collection_it_current(zend_object_iterator *obj_iter) {
	// DUMP("TraversableTest::current\n");
	php_csv_lazy_collection_it *iterator = php_csv_lazy_collection_it_fetch(obj_iter);
	php_csv_lazy_collection_object *lazy_collection = php_csv_lazy_collection_object_fetch(&iterator->intern.data);

	if (UNEXPECTED(Z_ISUNDEF(lazy_collection->current_row))) {
		return NULL;
	}
	return &lazy_collection->current_row;
}

static const zend_object_iterator_funcs php_csv_lazy_collection_it_vtable = {
	php_csv_lazy_collection_it_dtor,
	php_csv_lazy_collection_it_valid,
	php_csv_lazy_collection_it_current,
	NULL, // get_current_key
	php_csv_lazy_collection_it_next,
	php_csv_lazy_collection_it_rewind,
	NULL, // invalidate_current
	NULL, // get_gc
};

static zend_object_iterator *php_csv_lazy_collection_get_iterator(
	zend_class_entry *ce,
	zval *object,
	int by_ref
) {
	if (by_ref) {
		zend_throw_error(NULL, "An iterator cannot be used with foreach by reference");
		return NULL;
	}

	php_csv_lazy_collection_it *iterator = emalloc(sizeof(php_csv_lazy_collection_it));
	zend_iterator_init((zend_object_iterator*)iterator);

	ZVAL_OBJ_COPY(&iterator->intern.data, Z_OBJ_P(object));
	iterator->intern.funcs = &php_csv_lazy_collection_it_vtable;

	return (zend_object_iterator*)iterator;
}

ZEND_METHOD(Csv_LazyLaxCollection, __construct) {
	ZEND_PARSE_PARAMETERS_NONE();

	zend_throw_error(NULL, "Cannot manually instantiate Csv\\LazyLaxCollection");
}

ZEND_METHOD(Csv_LazyLaxCollection, getIterator) {
	ZEND_PARSE_PARAMETERS_NONE();

	zend_create_internal_iterator_zval(return_value, ZEND_THIS);
}

/* See static void buffer_to_collection_generic */
ZEND_METHOD(Csv_LazyLaxCollection, createFromBuffer) {
	zend_string *delimiter = NULL;
	zend_string *enclosure = NULL;
	zend_string *eol_sequence = NULL;
	zend_string *buffer;

	if (zend_parse_parameters(ZEND_NUM_ARGS(), "S|SSS", &buffer, &delimiter, &enclosure, &eol_sequence) == FAILURE) {
		RETURN_THROWS();
	}

	EOL_SEQUENCE_AND_DELIMITER_AND_ENCLOSURE_CHECKS(2, 3, 4);

	object_init_ex(return_value, php_csv_lazy_collection_ce);
	php_csv_lazy_collection_object *lazy_collection = php_csv_lazy_collection_object_fetch(return_value);

	/* We have copies from the macro */
	lazy_collection->eol_sequence = eol_sequence;
	lazy_collection->enclosure = enclosure;
	lazy_collection->delimiter = delimiter;
	lazy_collection->buffer = buffer;
}

ZEND_MINIT_FUNCTION(csv)
{
	php_csv_lazy_collection_ce = register_class_Csv_LazyLaxCollection(zend_ce_aggregate);
	php_csv_lazy_collection_ce->create_object = php_csv_lazy_collection_object_new;
	php_csv_lazy_collection_ce->get_iterator = php_csv_lazy_collection_get_iterator;

#if PHP_VERSION_ID >= 80300
	// default object handlers field only exists as of PHP 8.3
	php_csv_lazy_collection_ce->default_object_handlers = &php_csv_lazy_collection_object_handlers;
#endif

	memcpy(&php_csv_lazy_collection_object_handlers, &std_object_handlers, sizeof(zend_object_handlers));
	php_csv_lazy_collection_object_handlers.offset = XtOffsetOf(php_csv_lazy_collection_object, std);
	php_csv_lazy_collection_object_handlers.free_obj = php_csv_lazy_collection_object_free;
	php_csv_lazy_collection_object_handlers.get_constructor = php_csv_lazy_collection_get_constructor;
	php_csv_lazy_collection_object_handlers.compare = zend_objects_not_comparable;
	php_csv_lazy_collection_object_handlers.clone_obj = NULL;

	return SUCCESS;
}

zend_module_entry csv_module_entry = {
	STANDARD_MODULE_HEADER,
	"csv",						/* Extension name */
	ext_functions,		/* zend_function_entry */
	ZEND_MINIT(csv),		/* PHP_MINIT - Module initialization */
	NULL,				/* PHP_MSHUTDOWN - Module shutdown */
	NULL,				/* PHP_RINIT - Request initialization */
	NULL,				/* PHP_RSHUTDOWN - Request shutdown */
	PHP_MINFO(csv),						/* PHP_MINFO - Module info */
	PHP_CSV_VERSION,				/* Version */
	STANDARD_MODULE_PROPERTIES
};

#ifdef COMPILE_DL_CSV
# ifdef ZTS
ZEND_TSRMLS_CACHE_DEFINE()
# endif
ZEND_GET_MODULE(csv)
#endif
