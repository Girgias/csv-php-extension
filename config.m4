dnl config.m4 for extension csv

dnl Comments in this file start with the string 'dnl'.
dnl Remove where necessary.

PHP_ARG_ENABLE([csv],
  [whether to enable csv support],
  [AS_HELP_STRING([--enable-csv],
    [Enable csv support])],
  [no])

if test "$PHP_CSV" != "no"; then
  dnl In case of no dependencies
  AC_DEFINE(HAVE_CSV, 1, [ Have csv support ])

  STD_CFLAGS="-std=c99"

  dnl Drop -Wpedantic due to "Zend/zend.h:117:3: error: ISO C99 doesn’t support unnamed structs/unions"
  MAINTAINER_CFLAGS="-Wall -Wextra"
  AX_CHECK_COMPILE_FLAG(-Wfatal-errors,         MAINTAINER_CFLAGS="$MAINTAINER_CFLAGS -Wfatal-errors")
  AX_CHECK_COMPILE_FLAG(-Werror,                MAINTAINER_CFLAGS="$MAINTAINER_CFLAGS -Werror")
  AX_CHECK_COMPILE_FLAG(-Wno-unused-parameter,  MAINTAINER_CFLAGS="$MAINTAINER_CFLAGS -Wno-unused-parameter")
  dnl Fast ZPP API uses shadow variables
  dnl AX_CHECK_COMPILE_FLAG(-Wshadow,               MAINTAINER_CFLAGS="$MAINTAINER_CFLAGS -Wshadow")
  AX_CHECK_COMPILE_FLAG(-Wdouble-promotion,     MAINTAINER_CFLAGS="$MAINTAINER_CFLAGS -Wdouble-promotion")
  AX_CHECK_COMPILE_FLAG(-Wformat=2,             MAINTAINER_CFLAGS="$MAINTAINER_CFLAGS -Wformat=2")
  AX_CHECK_COMPILE_FLAG(-Wformat-truncation,    MAINTAINER_CFLAGS="$MAINTAINER_CFLAGS -Wformat-truncation")
  AX_CHECK_COMPILE_FLAG(-Wduplicated-cond,      MAINTAINER_CFLAGS="$MAINTAINER_CFLAGS -Wduplicated-cond")
  AX_CHECK_COMPILE_FLAG(-Wduplicated-branches,  MAINTAINER_CFLAGS="$MAINTAINER_CFLAGS -Wduplicated-branches")
  AX_CHECK_COMPILE_FLAG(-Wlogical-op,           MAINTAINER_CFLAGS="$MAINTAINER_CFLAGS -Wlogical-op")
  AX_CHECK_COMPILE_FLAG(-Wrestrict,             MAINTAINER_CFLAGS="$MAINTAINER_CFLAGS -Wrestrict")
  AX_CHECK_COMPILE_FLAG(-Wnull-dereference,     MAINTAINER_CFLAGS="$MAINTAINER_CFLAGS -Wnull-dereference")
  AX_CHECK_COMPILE_FLAG(-Wlogical-not-parentheses,  MAINTAINER_CFLAGS="$MAINTAINER_CFLAGS -Wlogical-not-parentheses")

  PHP_CSV_CFLAGS="$STD_CFLAGS $MAINTAINER_CFLAGS"

  PHP_NEW_EXTENSION(csv, csv.c, $ext_shared,, $PHP_CSV_CFLAGS)
fi
