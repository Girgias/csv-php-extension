/*
   +----------------------------------------------------------------------+
   | Copyright (c) The PHP Group                                          |
   +----------------------------------------------------------------------+
   | This source file is subject to version 3.01 of the PHP license,      |
   | that is bundled with this package in the file LICENSE, and is        |
   | available through the world-wide-web at the following url:           |
   | http://www.php.net/license/3_01.txt                                  |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
   | Authors: Gina Peter Banyard <girgias@php.net>                        |
   +----------------------------------------------------------------------+
*/
/* csv extension for PHP */

#ifndef PHP_CSV_H
# define PHP_CSV_H

/* Forward compatibility with PHP 8.1. */
# ifndef ZEND_ACC_NOT_SERIALIZABLE
#  define ZEND_ACC_NOT_SERIALIZABLE (1 << 29)
# endif

extern zend_module_entry csv_module_entry;
# define phpext_csv_ptr &csv_module_entry

# define PHP_CSV_NAME "CSV"
# define PHP_CSV_VERSION "0.5.0"
# define PHP_CSV_URL "https://gitlab.com/Girgias/csv-php-extension"
# define PHP_CSV_BUGREPORT "https://gitlab.com/Girgias/csv-php-extension/issues"

# if defined(ZTS) && defined(COMPILE_DL_CSV)
ZEND_TSRMLS_CACHE_EXTERN()
# endif

#endif	/* PHP_CSV_H */
