<?xml version="1.0"?>
<!DOCTYPE package SYSTEM "http://pear.php.net/dtd/package-1.0">
<package xmlns="http://pear.php.net/dtd/package-2.0"
         xmlns:tasks="http://pear.php.net/dtd/tasks-1.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         packagerversion="1.4.11" version="2.0"
         xsi:schemaLocation="http://pear.php.net/dtd/tasks-1.0 http://pear.php.net/dtd/tasks-1.0.xsd http://pear.php.net/dtd/package-2.0 http://pear.php.net/dtd/package-2.0.xsd">
    <name>CSV</name>
    <channel>pecl.php.net</channel>
    <summary>CSV PHP extension</summary>
    <description>
        A new and improved CSV file PHP extension which follows RFC 4180 instead of using a custom escape mechanism.

        Supports multi-bytes delimiters, enclosures, and providing a custom EOL sequence.

        Sponsor me via GitHub sponsors: https://github.com/sponsors/Girgias
    </description>
    <lead>
        <name>Gina Peter Banyard</name>
        <user>girgias</user>
        <email>girgias@php.net</email>
        <active>yes</active>
    </lead>

    <date>2025-02-22</date>
    <version>
        <release>0.4.3</release>
        <api>0.4.0</api>
    </version>
    <stability>
        <release>alpha</release>
        <api>alpha</api>
    </stability>
    <license uri="https://www.php.net/license/3_01.txt">PHP V3.01</license>
    <notes>
        * Fix a memory leak
        * Use standard error phrasing for error messages

        Sponsor me via GitHub sponsors: https://github.com/sponsors/Girgias
    </notes>
    <contents>
        <dir name="/">
            <file role="doc" name="LICENSE" />
            <file role="doc" name="README.md" />
            <file role="src" name="config.m4" />
            <file role="src" name="config.w32" />
            <file role="src" name="csv.c" />
            <file role="src" name="csv.stub.php" />
            <file role="src" name="csv_arginfo.h" />
            <file role="src" name="php_csv.h" />
            <file role="test" name="tests/001_load-csv-extension-check.phpt" />
            <file role="test" name="tests/002_instantiate_CSV_class.phpt" />
            <file role="test" name="tests/array_to_row/basic.phpt" />
            <file role="test" name="tests/array_to_row/error.phpt" />
            <file role="test" name="tests/array_to_row/multi_byte_delimiter.phpt" />
            <file role="test" name="tests/array_to_row/multi_byte_enclosure.phpt" />
            <file role="test" name="tests/array_to_row/non_stringable_elements.phpt" />
            <file role="test" name="tests/array_to_row/nul_byte_custom_eol_sequence.phpt" />
            <file role="test" name="tests/array_to_row/nul_byte_delimiter.phpt" />
            <file role="test" name="tests/array_to_row/nul_byte_enclosure.phpt" />
            <file role="test" name="tests/array_to_row/with_custom_eol_sequence.phpt" />
            <file role="test" name="tests/array_to_row/with_new_lines.phpt" />
            <file role="test" name="tests/array_to_row/with_nul_bytes.phpt" />
            <file role="test" name="tests/buffer_to_collection/basic.phpt" />
            <file role="test" name="tests/buffer_to_collection/errors.phpt" />
            <file role="test" name="tests/buffer_to_collection/non_escaped_enclosure.phpt" />
            <file role="test" name="tests/buffer_to_collection/nul_byte_custom_eol_sequence.phpt" />
            <file role="test" name="tests/buffer_to_collection/with_custom_eol_sequence.phpt" />
            <file role="test" name="tests/buffer_to_collection/with_new_lines.phpt" />
            <file role="test" name="tests/buffer_to_collection_lax/non_escaped_enclosure.phpt" />
            <file role="test" name="tests/buffer_to_collection_lax/with_uneven_rows.phpt" />
            <file role="test" name="tests/collection_to_buffer/basic.phpt" />
            <file role="test" name="tests/collection_to_buffer/basic_with_generator.phpt" />
            <file role="test" name="tests/collection_to_buffer/basic_with_iterator.phpt" />
            <file role="test" name="tests/collection_to_buffer/basic_with_iterator_aggregate.phpt" />
            <file role="test" name="tests/collection_to_buffer/errors.phpt" />
            <file role="test" name="tests/collection_to_buffer/non_stringable_elements.phpt" />
            <file role="test" name="tests/collection_to_buffer/with_custom_eol_sequence.phpt" />
            <file role="test" name="tests/csv_functions_inverses.phpt" />
            <file role="test" name="tests/deny_instantiation.phpt" />
            <file role="test" name="tests/enclosure_buffer_overflow.phpt" />
            <file role="test" name="tests/row_to_array/basic.phpt" />
            <file role="test" name="tests/row_to_array/error.phpt" />
            <file role="test" name="tests/row_to_array/multi_byte_delimiter.phpt" />
            <file role="test" name="tests/row_to_array/multi_byte_enclosure.phpt" />
            <file role="test" name="tests/row_to_array/non_escaped_enclosure.phpt" />
            <file role="test" name="tests/row_to_array/nul_byte_custom_eol_sequence.phpt" />
            <file role="test" name="tests/row_to_array/nul_byte_delimiter.phpt" />
            <file role="test" name="tests/row_to_array/nul_byte_enclosure.phpt" />
            <file role="test" name="tests/row_to_array/unicode_bom.phpt" />
            <file role="test" name="tests/row_to_array/with_custom_eol_sequence.phpt" />
            <file role="test" name="tests/row_to_array/with_new_lines.phpt" />
            <file role="test" name="tests/row_to_array/with_new_lines_strict_compliance.phpt" />
        </dir>
    </contents>
    <dependencies>
        <required>
            <php>
                <min>8.0.0</min>
            </php>
            <pearinstaller>
                <min>1.4.0</min>
            </pearinstaller>
        </required>
    </dependencies>
    <providesextension>CSV</providesextension>
    <extsrcrelease/>
    <changelog>
        <release>
            <date>2025-02-22</date>
            <version>
                <release>0.4.3</release>
                <api>0.4.0</api>
            </version>
            <stability>
                <release>alpha</release>
                <api>alpha</api>
            </stability>
            <license uri="https://www.php.net/license/3_01.txt">PHP V3.01</license>
            <notes>
                * Fix a memory leak
                * Use standard error phrasing for error messages
            </notes>
        </release>
        <release>
            <date>2022-05-31</date>
            <version>
                <release>0.4.2</release>
                <api>0.4.0</api>
            </version>
            <stability>
                <release>alpha</release>
                <api>alpha</api>
            </stability>
            <license uri="https://www.php.net/license/3_01.txt">PHP V3.01</license>
            <notes>
                Security release.

                Fix buffer overflow when using multibyte enclosures
            </notes>
        </release>
        <release>
            <date>2022-05-31</date>
            <version>
                <release>0.3.2</release>
                <api>0.3.0</api>
            </version>
            <stability>
                <release>alpha</release>
                <api>alpha</api>
            </stability>
            <license uri="https://www.php.net/license/3_01.txt">PHP V3.01</license>
            <notes>
                Security release.

                Fix buffer overflow when using multibyte enclosures
            </notes>
        </release>
        <release>
            <date>2021-09-05</date>
            <version>
                <release>0.4.1</release>
                <api>0.4.0</api>
            </version>
            <stability>
                <release>alpha</release>
                <api>alpha</api>
            </stability>
            <license uri="https://www.php.net/license/3_01.txt">PHP V3.01</license>
            <notes>
                * Compatibility with PHP 8.1.

                Sponsor me via GitHub sponsors: https://github.com/sponsors/Girgias
            </notes>
        </release>
        <release>
            <date>2021-02-22</date>
            <version>
                <release>0.4.0</release>
                <api>0.4.0</api>
            </version>
            <stability>
                <release>alpha</release>
                <api>alpha</api>
            </stability>
            <license uri="https://www.php.net/license/3_01.txt">PHP V3.01</license>
            <notes>
                * Minimum PHP version is now 8.0 due to the usage of new Zend APIs.
                * All functions have been moved to static methods of a new CSV class.
                * Function names now refer to "buffer" instead of "file" as a string is used.
                * CSV::collectionToBuffer() now supports iterable collections, not just arrays.
                * In case of field number miss-match a ValueError is thrown instead of an Error.
                * A new CSV::bufferToCollectionLax() has been introduced which doesn't throw a ValueError in case of field number miss-match.

                Sponsor me via GitHub sponsors: https://github.com/sponsors/Girgias
            </notes>
        </release>
        <release>
            <date>2020-03-05</date>
            <version>
                <release>0.3.1</release>
                <api>0.3.1</api>
            </version>
            <stability>
                <release>alpha</release>
                <api>alpha</api>
            </stability>
            <license uri="https://www.php.net/license/3_01.txt">PHP V3.01</license>
            <notes>
                Fix Windows compilation.
                Fix package stability.
                Add tests for a custom EOL sequence as nul byte.
                Bundle tests with release.
            </notes>
        </release>
        <release>
            <date>2020-03-04</date>
            <version>
                <release>0.3.0</release>
                <api>0.3.0</api>
            </version>
            <stability>
                <release>alpha</release>
                <api>alpha</api>
            </stability>
            <license uri="https://www.php.net/license/3_01.txt">PHP V3.01</license>
            <notes>
                Initial release on PECL.
            </notes>
        </release>
    </changelog>
</package>
