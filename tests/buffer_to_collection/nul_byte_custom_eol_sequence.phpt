--TEST--
Test Csv\buffer_to_collection() with custom EOL sequence as a nul byte
--SKIPIF--
<?php
if (!extension_loaded('csv')) {
	echo 'skip';
}
?>
--FILE--
<?php

$collection = [
    [
        'One',
        'Two',
        'Three',
    ],
    [
        'Un',
        'Deux',
        'Trois',
    ],
    [
        'Ichi',
        'Ni',
        'San',
    ],
];

$string = "One,Two,Three\0Un,Deux,Trois\0Ichi,Ni,San\0";

var_dump($collection === Csv\buffer_to_collection($string, ',', '"', "\0"));
var_dump(Csv\buffer_to_collection($string, ',', '"', "\0"));

?>
--EXPECT--
bool(true)
array(3) {
  [0]=>
  array(3) {
    [0]=>
    string(3) "One"
    [1]=>
    string(3) "Two"
    [2]=>
    string(5) "Three"
  }
  [1]=>
  array(3) {
    [0]=>
    string(2) "Un"
    [1]=>
    string(4) "Deux"
    [2]=>
    string(5) "Trois"
  }
  [2]=>
  array(3) {
    [0]=>
    string(4) "Ichi"
    [1]=>
    string(2) "Ni"
    [2]=>
    string(3) "San"
  }
}
