--TEST--
Test Csv\buffer_to_collection() with standard parameters
--SKIPIF--
<?php
if (!extension_loaded('csv')) {
	echo 'skip';
}
?>
--FILE--
<?php

$collection = [
    [
        'One',
        "I have a\n new line",
        'Three',
    ],
    [
        'Un',
        "I have a\r carriage return",
        'Trois',
    ],
    [
        'Ichi',
        "I have a carriage\r\n return new line",
        'San',
    ],
];

$string = "One,\"I have a\n new line\",Three\r\nUn,\"I have a\r carriage return\",Trois\r\nIchi,\"I have a carriage\r\n return new line\",San\r\n";

var_dump($collection === Csv\buffer_to_collection($string));

?>
--EXPECT--
bool(true)
