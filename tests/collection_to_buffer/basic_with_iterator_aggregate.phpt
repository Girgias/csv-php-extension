--TEST--
Test Csv\collection_to_buffer() with a IteratorAggregate as a collection
--SKIPIF--
<?php
if (!extension_loaded('csv')) {
	echo 'skip';
}
?>
--FILE--
<?php

class Collection implements IteratorAggregate {
    public $property1 = ['One', 'Two', 'Three'];
    public $property2 = ['Un', 'Deux', 'Trois'];
    public $property3 = ['Ichi', 'Ni', 'San'];

    public function __construct() {
        /* Suppress deprecation notice about dynamic property creation */
        @$this->property4 = ['Eins', 'Zwei', 'Drei'];
    }

    public function getIterator(): ArrayIterator {
        return new ArrayIterator($this);
    }
}

$collection = new Collection();

$output = "One,Two,Three\r\nUn,Deux,Trois\r\nIchi,Ni,San\r\nEins,Zwei,Drei\r\n";

var_dump($output === Csv\collection_to_buffer($collection));
var_dump(Csv\collection_to_buffer($collection));
var_dump(Csv\collection_to_buffer($collection, ','));
var_dump(Csv\collection_to_buffer($collection, ',', '"'));
var_dump(Csv\collection_to_buffer($collection, ',', '"', "\r\n"));

?>
--EXPECT--
bool(true)
string(59) "One,Two,Three
Un,Deux,Trois
Ichi,Ni,San
Eins,Zwei,Drei
"
string(59) "One,Two,Three
Un,Deux,Trois
Ichi,Ni,San
Eins,Zwei,Drei
"
string(59) "One,Two,Three
Un,Deux,Trois
Ichi,Ni,San
Eins,Zwei,Drei
"
string(59) "One,Two,Three
Un,Deux,Trois
Ichi,Ni,San
Eins,Zwei,Drei
"
