--TEST--
Test Csv\collection_to_buffer() with standard parameters
--SKIPIF--
<?php
if (!extension_loaded('csv')) {
	echo 'skip';
}
?>
--FILE--
<?php

$collection = [
    [
        'One',
        'Two',
        'Three',
    ],
    [
        'Un',
        'Deux',
        'Trois',
    ],
    [
        'Ichi',
        'Ni',
        'San',
    ],
];

$output = "One,Two,Three\r\nUn,Deux,Trois\r\nIchi,Ni,San\r\n";

var_dump($output === Csv\collection_to_buffer($collection));
var_dump(Csv\collection_to_buffer($collection));
var_dump(Csv\collection_to_buffer($collection, ','));
var_dump(Csv\collection_to_buffer($collection, ',', '"'));
var_dump(Csv\collection_to_buffer($collection, ',', '"', "\r\n"));

?>
--EXPECT--
bool(true)
string(43) "One,Two,Three
Un,Deux,Trois
Ichi,Ni,San
"
string(43) "One,Two,Three
Un,Deux,Trois
Ichi,Ni,San
"
string(43) "One,Two,Three
Un,Deux,Trois
Ichi,Ni,San
"
string(43) "One,Two,Three
Un,Deux,Trois
Ichi,Ni,San
"
