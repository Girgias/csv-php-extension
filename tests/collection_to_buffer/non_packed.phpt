--TEST--
Test Csv\collection_to_buffer() with non-packed array
--SKIPIF--
<?php
if (!extension_loaded('csv')) {
	echo 'skip';
}
?>
--FILE--
<?php

$collection = [
    'en' => [
        'One',
        'Two',
        'Three',
    ],
    'fr' => [
        'Un',
        'Deux',
        'Trois',
    ],
    'ja' => [
        'Ichi',
        'Ni',
        'San',
    ],
];

$output = "One,Two,Three\r\nUn,Deux,Trois\r\nIchi,Ni,San\r\n";

var_dump($output === Csv\collection_to_buffer($collection));
var_dump(Csv\collection_to_buffer($collection));

?>
--EXPECT--
bool(true)
string(43) "One,Two,Three
Un,Deux,Trois
Ichi,Ni,San
"
