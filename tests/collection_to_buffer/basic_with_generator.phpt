--TEST--
Test Csv\collection_to_buffer() with a Generator as a collection
--SKIPIF--
<?php
if (!extension_loaded('csv')) {
	echo 'skip';
}
?>
--FILE--
<?php

$collection = [
    [
        'One',
        'Two',
        'Three',
    ],
    [
        'Un',
        'Deux',
        'Trois',
    ],
    [
        'Ichi',
        'Ni',
        'San',
    ],
];

function getFields($collection) {
    yield from $collection;
};

$output = "One,Two,Three\r\nUn,Deux,Trois\r\nIchi,Ni,San\r\n";

var_dump($output === Csv\collection_to_buffer(getFields($collection)));
var_dump(Csv\collection_to_buffer(getFields($collection)));
var_dump(Csv\collection_to_buffer(getFields($collection), ','));
var_dump(Csv\collection_to_buffer(getFields($collection), ',', '"'));
var_dump(Csv\collection_to_buffer(getFields($collection), ',', '"', "\r\n"));

?>
--EXPECT--
bool(true)
string(43) "One,Two,Three
Un,Deux,Trois
Ichi,Ni,San
"
string(43) "One,Two,Three
Un,Deux,Trois
Ichi,Ni,San
"
string(43) "One,Two,Three
Un,Deux,Trois
Ichi,Ni,San
"
string(43) "One,Two,Three
Un,Deux,Trois
Ichi,Ni,San
"
