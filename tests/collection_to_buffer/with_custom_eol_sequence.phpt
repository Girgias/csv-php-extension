--TEST--
Test Csv\collection_to_buffer() with standard parameters
--SKIPIF--
<?php
if (!extension_loaded('csv')) {
	echo 'skip';
}
?>
--FILE--
<?php

$collection = [
    [
        'One',
        'Two',
        'Three',
    ],
    [
        'Un',
        'Deux',
        'Trois',
    ],
    [
        'Ichi',
        'Ni',
        'San',
    ],
];

$output = "One,Two,ThreeあUn,Deux,TroisあIchi,Ni,Sanあ";

var_dump($output === Csv\collection_to_buffer($collection, ',', '"', 'あ'));
var_dump(Csv\collection_to_buffer($collection, ',', '"', 'あ'));

?>
--EXPECT--
bool(true)
string(46) "One,Two,ThreeあUn,Deux,TroisあIchi,Ni,Sanあ"
