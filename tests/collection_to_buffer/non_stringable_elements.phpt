--TEST--
Test Csv\collection_to_buffer() with a row not containing string elements
--SKIPIF--
<?php
if (!extension_loaded('csv')) {
	echo 'skip';
}
?>
--FILE--
<?php

$collection = [
    [
        'One',
        'Two',
        'Three',
    ],
    [
        'Un',
        ['Not a string'],
        'Trois',
    ],
    [
        'Ichi',
        'Ni',
        'San',
    ],
];

try {
    var_dump(Csv\collection_to_buffer($collection));
} catch (\Throwable $e) {
    echo $e::class, $e->getMessage(), \PHP_EOL;
}

$collection = [
    [
        'One',
        'Two',
        'Three',
    ],
    [
        'Un',
        new stdClass(),
        'Trois',
    ],
    [
        'Ichi',
        'Ni',
        'San',
    ],
];

try {
    var_dump(Csv\collection_to_buffer($collection));
} catch (\Throwable $e) {
    echo $e::class, $e->getMessage(), \PHP_EOL;
}

?>
--EXPECTF--
Warning: Array to string conversion in %s on line %d
string(44) "One,Two,Three
Un,Array,Trois
Ichi,Ni,San
"
ErrorObject of class stdClass could not be converted to string
