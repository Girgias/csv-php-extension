--TEST--
Csv\LazyLaxCollection::createFromBuffer() with new lines in buffer
--SKIPIF--
<?php
if (!extension_loaded('csv')) {
	echo 'skip';
}
?>
--FILE--
<?php

$collection = [
    [
        'One',
        "I have a\n new line",
        'Three',
    ],
    [
        'Un',
        "I have a\r carriage return",
        'Trois',
    ],
    [
        'Ichi',
        "I have a carriage\r\n return new line",
        'San',
    ],
];

$string = "One,\"I have a\n new line\",Three\r\nUn,\"I have a\r carriage return\",Trois\r\nIchi,\"I have a carriage\r\n return new line\",San\r\n";

$it = Csv\LazyLaxCollection::createFromBuffer($string);

foreach ($it as $nb => $row) {
    var_dump($row === $collection[$nb]);
}

?>
--EXPECT--
bool(true)
bool(true)
bool(true)
