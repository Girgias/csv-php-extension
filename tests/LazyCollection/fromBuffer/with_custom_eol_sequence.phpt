--TEST--
Csv\LazyLaxCollection::createFromBuffer() with standard parameters
--SKIPIF--
<?php
if (!extension_loaded('csv')) {
	echo 'skip';
}
?>
--FILE--
<?php

$collection = [
    [
        'One',
        "Two",
        'Three',
    ],
    [
        'Un',
        "Deux",
        'Trois',
    ],
    [
        'Ichi',
        "Ni",
        'San',
    ],
];

$string = "One,Two,ThreeあUn,Deux,TroisあIchi,Ni,Sanあ";

$it = Csv\LazyLaxCollection::createFromBuffer($string, ',', '"', 'あ');

foreach ($it as $nb => $row) {
    var_dump($row === $collection[$nb]);
}

?>
--EXPECT--
bool(true)
bool(true)
bool(true)
