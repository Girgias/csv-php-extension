--TEST--
Error conditions for Csv\buffer_to_collection_lax()
--SKIPIF--
<?php
if (!extension_loaded('csv')) {
	echo 'skip';
}
?>
--FILE--
<?php

$collection = [
    [
        'One',
        'Two',
        'Three',
    ],
    [
        'Un',
        'Deux',
    ],
    [
        'Ichi',
        'Ni',
        'San',
    ],
];

$string = "One,Two,Three\r\nUn,Deux\r\nIchi,Ni,San\r\n";

var_dump($collection === Csv\buffer_to_collection_lax($string));
var_dump(Csv\buffer_to_collection_lax($string));

?>
--EXPECT--
bool(true)
array(3) {
  [0]=>
  array(3) {
    [0]=>
    string(3) "One"
    [1]=>
    string(3) "Two"
    [2]=>
    string(5) "Three"
  }
  [1]=>
  array(2) {
    [0]=>
    string(2) "Un"
    [1]=>
    string(4) "Deux"
  }
  [2]=>
  array(3) {
    [0]=>
    string(4) "Ichi"
    [1]=>
    string(2) "Ni"
    [2]=>
    string(3) "San"
  }
}
